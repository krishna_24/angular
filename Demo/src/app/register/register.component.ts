import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  empName: any;
  salary: any;
  gender: any;
  doj: any;
  phoneNumber: any;
  emailId: any;
  password: any;
  countries: any;

  constructor(private router: Router, private toastr: ToastrService, private Service: EmpService) {
  }

  ngOnInit() {
    this.Service.getAllCountries().subscribe((data: any) => {
      this.countries = data;
      console.log(data);
    });
  }

  submit() {
    console.log("EmpName: " + this.empName);
    console.log("Salary: " + this.salary);
    console.log("Gender: " + this.gender);
    console.log("DateOfJoin: " + this.doj);
    console.log("Country: " + this.countries);
    console.log("PhoneNumber: " + this.phoneNumber);
    console.log("Email-Id: " + this.emailId);
    console.log("Password: " + this.password);
  }

  registerSubmit(regForm: any) {
console.log(regForm)
    if (!this.validateForm()) {
      return;
    }
    const newEmployee = {
      empId: this.generateEmpId(),
      empName: this.empName,
      salary: this.salary,
      gender: this.gender,
      doj: this.doj,
      country: this.countries,
      phoneNumber: this.phoneNumber,
      emailId: this.emailId,
      password: this.password
    };

    console.log(newEmployee);

    this.toastr.success('Registration Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 3000, // 3 seconds
    });

    this.router.navigate(['login']);
  }

  private validateForm(): boolean {

    if (!this.empName || !this.salary || !this.emailId || !this.password) {
      this.toastr.error('Please fill in all required fields.', 'Error', {
        closeButton: true,
        progressBar: true,
        positionClass: 'toast-top-right',
        tapToDismiss: false,
        timeOut: 3000, // 3 seconds
      });
      return false;
    }
    return true;
  }

  private generateEmpId(): number {
    return Math.floor(Math.random() * 1000) + 1;
  }
}