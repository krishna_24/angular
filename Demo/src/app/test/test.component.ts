import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit {
  id:number;
  name:string;
  avg:number;

  address:any;

  hobbies:any;

  constructor(){
    //alert("constructor is invoked");
    this.id = 101;
    this.name = 'vamshi';
    this.avg = 99.98;

    this.address = {
     streetno:101,
     city:'hyderabad',
     state:'telangan'
    };

    this.hobbies = ['running','eating','reading'];

  }
   ngOnInit(){
    //alert("ngOnInit is invoked");
   }
}
