import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {
  
  employees: any;
  employee:any;

  constructor(private service : EmpService) {
    this.employees = [
      {empId: 101, empName:'Harsha', salary:1212.12, gender:'Male',   doj:'2018-11-15', country:'India',    emailId:'harsha@gmail.com', password:'123'},
      {empId: 102, empName:'uday',  salary:2323.23, gender:'Male',   doj:'2017-10-16', country:'China',    emailId:'uday@gmail.com',  password:'123'},
      {empId: 103, empName:'Rajesh', salary:3434.34, gender:'Male', doj:'2016-09-17', country:'USA',      emailId:'rajesh@gmail.com', password:'123'},
      {empId: 104, empName:'Vamsi',  salary:4545.45, gender:'Male',   doj:'2015-08-18', country:'SriLanka', emailId:'vamsi@gmail.com',  password:'123'},
      {empId: 105, empName:'Ajay', salary:5656.56, gender:'Male',   doj:'2014-07-19', country:'Nepal',    emailId:'ajay@gmail.com', password:'123'}
    ];
  }

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) => {
      this.employee = data;
      console.log(data);
  });

  }

  

  

}